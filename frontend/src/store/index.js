import Vue from 'vue'
import Vuex from 'vuex'
import rest from "../plugins/rest.api";

/**
 * Vuex input file, main functions, authorization and error text are described here
 */

Vue.use(Vuex)

import pathViewer from './path-viewer'

export default new Vuex.Store({
	state: {
		errorMessage: "",
		accessToken: undefined || localStorage.getItem("accessToken")
	},
	getters: {
		errorMessage: (state) => state.errorMessage,
		accessToken: (state) => state.accessToken
	},
	mutations: {
		setErrorMessage(state, message) {
			state.errorMessage = message;
		},
		auth(state, token) {
			localStorage.setItem("accessToken", token);
			rest.defaults.headers["Authorization"] = `Bearer ${token}`;
			state.accessToken = token;
		},
		logout(state) {
			localStorage.removeItem("accessToken");
			rest.defaults.headers["Authorization"] = undefined;
			state.accessToken = false;
		}
	},
	actions: {
		// Entries should be cleaned up to avoid a visual bug on the next login
		logoutAndClearEntries({ commit }) {
			commit("logout");
			commit("entriesReplace", []);
		}
	},
	modules: {
		pathViewer
	}
})
