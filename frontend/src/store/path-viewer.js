import rest from "../plugins/rest.api";

/**
 * This file describes interactions with explorer state.
 * Entries are all files that are loaded from the server, then you can make mutations with it, which are described in the mutation block.
 * UploadingFiles is the upload queue, interactions with it are also described in the mutation block.
 * RemovingFiles is the removal queue, interactions with it are also described in the mutation block.
 * Loading is designed to display loading in the explorer itself, it can be turned on when pulling it from the directory server, the interaction is described in the actions block.
 *
 * All the names of mutations are intuitive, I see no point in describing their work.
 */

export default {
	state: {
		entries: [],
		uploadingFiles: [],
		removingFiles: [],
		loading: false
	},
	getters: {
		entries: (state) => state.entries,
		uploadingFiles: (state) => state.uploadingFiles,
		removingFiles: (state) => state.removingFiles,
		explorerLoading: (state) => state.loading
	},
	mutations: {
		pushEntries(state, data) {
			state.entries.push(data);
		},
		entriesReplace(state, data) {
			state.entries = data;
		},
		removeFromEntries(state, id) {
			let index = state.entries.findIndex(x => x.id === id);
			if (index > -1) {
				state.entries.splice(index, 1);
			}
		},
		pushUploading(state, data) {
			state.uploadingFiles.push(data);
		},
		removeFromUploading(state, lm) {
			let index = state.uploadingFiles.findIndex(x => x.lastModified === lm);
			if (index > -1) {
				state.uploadingFiles.splice(index, 1);
			}
		},
		pushRemoving(state, data) {
			if (!state.removingFiles.includes(data)) state.removingFiles.push(data);
		},
		removeFromRemoving(state, id) {
			let index = state.removingFiles.findIndex(x => x.id === id);
			if (index > -1) {
				state.removingFiles.splice(index, 1);
			}
		},
		/**
		 * This mutation still needs to be described, 
		 * it is intended for mass selection of files from the directory, 
		 * if the object has entries selected == true, 
		 * then you can filter it and, for example, delete the selected objects
		 */
		selectAll(state, bool) {
			state.entries.map(x => x.selected = bool);
		}
	},
	actions: {
		/**
		 * This action is intended to remove a file from the server, 
		 * as you can see, the function selects all selected objects from entries and first removes them from the server, 
		 * then from the explorer, while writing all this to the queue 
		 */
		removeEntries({ state, commit }) {
			const entries = state.entries;
			const selected = entries.filter(x => x.selected === true);
			selected.forEach(x => {
				commit("pushRemoving", x);
				rest.post("/path/delete", { path: x.path_lower }).then(() => {
					commit("removeFromEntries", x.id);
					commit("removeFromRemoving", x.id);
				});
			});
		},
		/**
		 * Everything is clear here, it takes data from the server and overwrites the entries array
		 */
		getEntries({ state, commit }, path) {
			state.loading = true;
			rest.post(`/path/list`, { path }).then((resp) => {
				let entries = resp.data.result.entries;
				commit(
					"entriesReplace",
					entries.map((x) => ({ ...x, selected: false }))
				);
				state.loading = false;
			});
		},
	}
}