import rest from "../plugins/rest.api";

export default {
	data: () => ({
	}),
	computed: {
		entries() {
			return this.$store.getters.entries;
		}
	},
	methods: {
		//This function is needed to detect repetitions of files in the directory, 
		// no repetition returns an object with the name of the file and the file itself, otherwise it changes the name
		checkAvailabilityAndMutate: function (file) {
			let sameEntries = this.entries.filter(x => x.name === file.name);
			if (sameEntries.length == 0) return {
				name: file.name,
				file: file
			};
			else return {
				name: `Copy-${sameEntries.length}-${file.name}`,
				file: file
			}
		},
		upload(arrFiles) {
			arrFiles.forEach((file) => {
				// pathWithoutExplorer - this variable is needed to correctly send the file position
				const pathWithoutExplorer = this.$route.path.replace("/explorer", "");
				// add file to upolaod queue
				this.$store.commit("pushUploading", file);
				let verifiedFile = this.checkAvailabilityAndMutate(file);

				const fd = new FormData();

				fd.append("path", file.webkitRelativePath.length === 0 ? pathWithoutExplorer : pathWithoutExplorer + "/" + file.webkitRelativePath);
				fd.append("name", verifiedFile.name);
				fd.append("file", verifiedFile.file);

				rest.post("upload", fd).then(() => {
					// romove file from upload queue before upload 
					this.$store.commit("removeFromUploading", file.lastModified);

					// after the download queue is empty, the state of the files in explorer is updated
					if (this.$store.getters.uploadingFiles.length === 0) this.$store.dispatch("getEntries", pathWithoutExplorer);
				}).catch(() => {
					// in case of an upload error, the file is removed from the queue
					this.$store.commit("removeFromUploading", file.lastModified);
				});
			});
		},
		uploadFile(event) {
			const files = event.target.files;
			const arrFiles = Array.from(files);
			if (arrFiles.length > 5) {
				this.$store.commit("setErrorMessage", "The Dropbox API does not allow you to upload more than 5 files in a row.");
				setTimeout(() => this.$store.commit("setErrorMessage", ""), 6000);
				this.upload(arrFiles.splice(0, 5));
			} else {
				this.upload(arrFiles);
			}
		},
		// this describes the minimal logic of dragging and dropping files into explorer and then uploading them to the server.
		// FOLDERS DO NOT LOAD, there was not enough time for this
		onDrop(e) {
			e.preventDefault();
			this.uploadFile({
				target: {
					files: e.dataTransfer.files,
				}
			});
		},
	},
};