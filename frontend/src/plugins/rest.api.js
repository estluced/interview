import axios from "axios";
import router from "../router/index.js";
import store from "../store/index.js";

let api = axios.create({
	baseURL: process.env.NODE_ENV === "development" ? "http://localhost:3000/api" : "/api",
	headers: {
		"Authorization": `Bearer ${store.getters.accessToken}`
	}
})

// The dropbox token has an expiration date when it expired, the dropbox api gives a 403 status code, 
// you need to reset the token when you get a 403 error and move the user to the authorization page
api.interceptors.response.use((resp) => resp, (error) => {
	if (error.response.status === 403) {
		store.commit("logoutAndClearEntries");
		router.push({ name: "Authorization" });
	}
	throw error;
});

export default api;