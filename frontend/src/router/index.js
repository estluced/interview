import Vue from 'vue'
import VueRouter from 'vue-router'
import Explorer from '../views/Explorer.vue'
import Authorization from "../views/Authorization.vue";
import store from "../store/index.js";

Vue.use(VueRouter)

// Authorization checks, page visibility validation
let ifAuth = (to, from, next) => {
	if (store.getters.accessToken) {
		next();
		return;
	} else {
		next("/auth");
	}
}

let ifNotAuth = (to, from, next) => {
	if (!store.getters.accessToken) {
		next();
		return;
	}
	next("/");
}

const routes = [
	{
		path: "/",
		redirect: "/explorer"
	},
	{
		path: "/auth",
		name: "Authorization",
		component: Authorization,
		beforeEnter: ifNotAuth
	},
	{
		path: '/explorer',
		name: 'ExplorerRoot',
		component: Explorer,
		beforeEnter: ifAuth
	},
	{
		path: '/explorer/:path+',
		name: 'Explorer',
		component: Explorer,
		beforeEnter: ifAuth
	}
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router
