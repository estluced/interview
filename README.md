# Dropbox UI

### Important!

### If you want to use your Dropbox API Token, you need to check some items in the app settings, see screenshot

![](./check-some-items.png)

## Project setup

```
cd frontend && yarn && yarn build
```

```
cd backend && yarn
```

### Starting server

### The default server starts at http://localhost:3000

```
cd backend && yarn start
```
