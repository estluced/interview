import { Dropbox } from "dropbox";
import axios from "axios";

export default function (token) {
	return new Dropbox({
		accessToken: token,
		axios
	});
}