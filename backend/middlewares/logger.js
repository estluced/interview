export default function (req, res, next) {
	const time = new Date();
	console.log(
		`[ ${req.method
		} | ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()} ] - ${req.path
		}`
	);
	next();
}