import dbx from "../dropbox.api.js";

// this middleware is needed to validate the token, 
// if the validation of the token is successful, the token is stored in request
export default function (req, res, next) {
	const bearerHeader = req.headers["authorization"];
	if (bearerHeader === undefined) {
		res.sendStatus(403);
	} else {
		const bearer = bearerHeader.split(" ");
		const token = bearer[1];
		// test request
		dbx(token).fileRequestsCount().then(() => {
			req.token = token;
			next();
		}).catch(() => {
			res.sendStatus(403);
		});
	}
}