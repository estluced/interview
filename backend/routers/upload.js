import { Router } from "express";
import os from "os";
import multer from "multer";
import dbx from "../dropbox.api.js";
import fs from "fs";
import verifyToken from "../middlewares/verifyToken.js";

const router = Router();
router.use(verifyToken);
const upload = multer({ dest: os.tmpdir() });

// It is important to note here that there are other types of file uploads to dropbox, I used this one due to lack of time.
// This type has its limitations, you cannot upload files larger than 50mb.
router.post("/upload", upload.single("file"), (req, res) => {
	const path = req.body["path"];
	const file = req.file;
	const name = req.body["name"];
	fs.readFile(file.path, function (error, content) {
		if (error) {
			res.status(500);
		} else {
			dbx(req.token).filesUpload({
				path: `${path}/${name}`,
				contents: content
			}).then(resp => {
				fs.unlinkSync(file.path);
				res.status(200);
				res.send(resp);
			}).catch((error) => {
				console.log(error)
				res.status(error.status);
				res.send(error);
			});
		}
	});
});

export default router;