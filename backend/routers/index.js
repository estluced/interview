import { Router, json } from "express";
import cors from "cors";
import upload from "./upload.js";
import path from "./path.js";
import auth from "./auth.js";
import logger from "../middlewares/logger.js";

const router = Router();

router.use(logger);
router.use(json());
router.use(cors());
router.use("/auth", auth);
router.use("/path", path);
router.use(upload);

export default router;