import { Router } from "express";
import dbx from "../dropbox.api.js";

const router = Router();

router.post("/verify-token", (req, res) => {
	const token = req.body["token"];
	dbx(token).fileRequestsCount().then(() => {
		res.status(200);
		res.send(token);
	}).catch((error) => {
		console.log(error)
		res.status(400);
		res.send(error);
	});
});

export default router;