import { Router } from "express";
import dbx from "../dropbox.api.js";
import verifyToken from "../middlewares/verifyToken.js";

const router = Router();
router.use(verifyToken);

router.post("/list", (req, res) => {
	const path = req.body.path;
	const correctPath = path === "/" ? "" : path;
	dbx(req.token).filesListFolder({ path: correctPath, recursive: false }).then((resp) => {
		res.status(200);
		res.json(resp);
	}).catch((error) => {
		res.status(error.status);
		res.send(error);
	})
});

router.post("/delete", (req, res) => {
	const path = req.body["path"];
	dbx(req.token).filesDeleteV2({ path }).then((resp) => {
		res.status(200);
		res.send(resp);
	}).catch((error) => {
		res.status(error.status);
		res.send(error);
	});
});

router.post("/create-folder", (req, res) => {
	const path = req.body["path"];
	dbx(req.token).filesCreateFolderV2({ path }).then((resp) => {
		res.status(200);
		res.send(resp);
	}).catch((error) => {
		res.status(error.status);
		res.send(error);
	});
});

export default router;