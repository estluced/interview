import express from "express";
import routers from "./routers/index.js"
import history from "connect-history-api-fallback";

const app = express();
const host = process.argv[2];
const port = process.argv[3];

app.use(history());
app.use(express.static("public"));
app.use("/api", routers);

app.listen(port, host, () => {
	console.log("App is start");
	console.log(`http://${host}:${port}`);
});